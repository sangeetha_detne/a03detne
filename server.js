var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express();  // make express app
app.use(express.static(__dirname + '/views'))
var server = require('http').createServer(app); // inject app into the server

// set up the view engine
// 1 set up the view engine
app.set("views", path.resolve(__dirname, "views")) // path to views
app.set("view engine", "ejs") // specify our view engine

// 2 create an array to manage our entries
var entries = []
app.locals.entries = entries // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"))     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }))

// GETS
// 4 handle http GET requests (default & /new-entry)
app.get("/", function (request, response) {
  response.sendFile(__dirname+"/views/index1.html")
})
app.get("/index1", function (request, response) {
  response.sendFile(__dirname+"/views/index1.html")
})
app.get("/new-entry", function (request, response) {
  response.render("new-entry")
})
app.get("/Contact",function(request, response){
  response.sendFile(__dirname+"/views/Contact.html")
})
app.get("/yourchoice",function(request, response){
  response.sendFile(__dirname+"/views/yourchoice.html")
})
app.get("/guestbook", function (request, response) {
  response.render("index")
})

// POSTS
// 5 handle an http POST request to the new-entry URI 

app.post("/Contact",function(request, response){
  var api_key = 'key-ba0920b4fd63707b6bbeb1fe381d013a';
  var domain = 'sandbox6abc4551f4194d4eb113f252f5292772.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: ' Mail gun from User <postmaster@sandbox6abc4551f4194d4eb113f252f5292772.mailgun.org>',
    to: 'S530668@nwmissouri.edu',
    subject: request.body.Name,
    text: 'Testing some Mailgun awesomness!\n'+"Message: "+request.body.message+"\n"+"From: "+request.body.Name
  };
   
  mailgun.messages().send(data, function (error, body) {
    if(!error){response.send("success")}else{response.send("failure")}
  });


})
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.")
    return
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  })
  response.redirect("/guestbook")  // where to go next? Let's go to the home page :)
})

// 404
// if we get a 404 status, render our 404.ejs view
app.use(function (request, response) {
  response.status(404).render("404")
})

// Listen for an application request on port 8081 & notify the developer
//http.listen(8081, function () {
 // console.log('Guestbook app listening on http://127.0.0.1:8081/')
//})

// Listen for an application request on port 8081
server.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/');
});
